<?php
session_start();

$logged_in = false;

if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == 1)
{
	$logged_in = true;
}

if(isset($_GET['u']))
{
	echo '<html><head><title>Login</title></head><body>';
	echo '<form action="./index.php" method="post">';
	echo '<input type="password" name="password" /><input type="Submit" value="Log In" /><input type="hidden" name="user" value="'.$_GET['u'].'"';
	echo '</body></html>';
	exit();
}

if(isset($_POST['password']))
{
	$pass = $_POST['password'];
	$user = $_POST['user'];
	
	if($user == 'lothario' && md5($pass) === 'cac58b5234e1f98b4c956998b8ac2e26')
	{
		$_SESSION['loggedin'] = 1;
		$logged_in = true;
	}
}


include ('./db.class.php');
include ('./engine.php');

header('Content-Type: text/html; charset=ISO-8859-10');

//Display
?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="style_ea.css" />
		<link rel="stylesheet" href="mobile.css" media="handheld" />
		<title>Fifa 13 Dane Tournament</title>
		<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-10" />
	</head>
	<body>
		

<?php

$html = array();

$html[] = '<h1 class="title"><img src="FIFA13_gdplogo.png" /><br /> Dane Tournament</h1>';

if($logged_in)
{
	$html[] = '<a href="./index.php?logout=true">Logout</a>';
}

$html[] = '<br /><a class="button" href="http://www.youtube.com/watch?v=3Xuax5qi2kc&list=PLwpWzg44xjFFbaNvdl627TczC5cx2yrFr&feature=plcp" target="_blank">View All Highlights</a><br />';
$html[] = '<div id="content"><div id="bg">';


foreach($stages as $id => $stage)
{
	//Calculate current stage
	$stageNo = $id+1;
	
	//Check if stage is grouped
	$sql = 'SELECT groupNo FROM fifa_teams
			WHERE stage = '.$stageNo.'
			ORDER BY stage, team_rating DESC, team_sort';
			
	$db->query($sql);

	$groupsExist = true;
	
	while($row = mysql_fetch_array($db->result))
	{
		if($row['groupNo'] == 0)
		{
			$groupsExist = false;
		}
	}
	
	//Check if next stage is grouped
	$sql = 'SELECT groupNo FROM fifa_teams
			WHERE stage = '.($stageNo +1).'
			ORDER BY stage, team_rating DESC, team_sort';
			
	$db->query($sql);

	$nextGroupsExist = true;
	
	while($row = mysql_fetch_array($db->result))
	{
		if($row['groupNo'] == 0)
		{
			$nextGroupsExist = false;
		}
	}
	
	if(isset($qualifiedTeams[($stageNo+1)]) && count($qualifiedTeams[($stageNo+1)]) > 0)
	{
		$nextGroupsExist = true;
	}
	
	if(!$groupsExist && (isset($qualifiedTeams[$stageNo]) && count($qualifiedTeams[$stageNo]) == $stage['qualify'] || $stage['qualify'] == 0))
	{
		$ready = true;
	}
	else
	{
		$ready = false;
	}
	
	
	
	$html[] = '<a name="'.$stageNo.'" class="bookmark"></a>';
	$html[] = '<div class="stage">';

	//Final stage
	if($stageNo == $numStages)
	{
		$header = 'Final Stage';
		$nextGroupsExist = false;
	}
	else
	{
		//This ensures 2 teams per 'group', hence knockout
		$qGroups = 2;
		
		//Make sure the penultimate stage is a group stage, not knockout
		if($stageNo == $numStages -1)
		{
			$qGroups = $qualTeamsPerGroup;
			$header = 'Final Qualifying Stage';
		}
		else
		{
			$header = 'Qualifying Stage: ' . ($stageNo);
		}
	}
	$html[] = '<h1 class="header">'.$header.'</h1>';
	
	if($ready)
	{
		$drawType = 'Draw';
	}
	
	if($groupsExist)
	{
		$drawType = 'Redraw';
	}
	
	if(($ready || $groupsExist) && !$nextGroupsExist && $logged_in)
	{
		$html[] = ' <a class="draw" href="./index.php?draw=' . $stageNo . '#'.$stageNo.'">'.$drawType.'</a>';
	}
	
	$html[] = '<table class="columns">';
	$html[] = '<tr><th class="main">Teams</th><th class="main">Structure</th></tr>';
	$html[] = '<tr>';
	$html[] = '<td class="main"><div class="rounded">';
	
	$html[] = '<h2 class="subheader"><span class="seeded">Seeded: ' .$stage['seeded'].'</span> | <span class="qualified">Qualified: '.$stage['qualify'].'</span></h2>';
	
	
	
	$html[] = ' <h3 class="title">Seeded Teams</h3> '; 
		
	foreach($seededTeams[$stageNo] as $team)
	{
		$html[] = '<span class="teamlist seeded">' . $team['team_name'] . '</span> ';
	};
			
	if($stageNo != 1)
	{
		$html[] = '<h3 class="title">Qualified Teams</h3> '; 
		
		if(!empty($qualifiedTeams[$stageNo]))
		{
			foreach($qualifiedTeams[$stageNo] as $team)
			{
				$html[] = '<span class="teamlist qualified">' . $team['team_name'] . '</span> ';
			}
		}
		else
		{
			$html[] = '<span class="pending">Pending</span>';
		}
	}
	$html[] = '</div></td><td class="main">';
	$html[] = '<table class="drawedteams">';
	
	$stageTotalTeams = $stage['seeded'] + $stage['qualify'];
	
	$html[] = '<tr>';
	$html[] = '<th>Draw</th>';
	$html[] = '<th>Teams</th>';
	$html[] = '<th>Matches</th>';
	$html[] = '</tr>';
	
	
	for($i=1; $i <= $stageTotalTeams / $qGroups; $i++)
	{
		$html[] = '<tr>';
		$html[] = '<td class="dt">'.$i.'</td>';
		$html[] = '<td class="dt">';
		$drawed_teams = '';
		$groupNo = $i;
		$matches = array();
		
		//Fetch matches for this stage
		$sql = 'SELECT * FROM fifa_matches
				WHERE group_id = ' . $groupNo . '
				AND stage = ' . $stageNo;
		
		$db->query($sql);
		while($row = mysql_fetch_array($db->result))
		{
			$matches[] = $row;
		}
		
		if($groupsExist)
		{
			//Check if the group qualifiers has already been found
			if(count($stageGroups[$stageNo][$i]) > $qGroups / 2)
			{
				foreach($stageGroups[$stageNo][$i] as $team)
				{
					if($team['qualified'] > 0)
					{
						$a_head = '<span class="qualified finalteams">';
					}
					else
					{
						$a_head = '<span class="seeded finalteams">';
					}
					$a_foot = '</span>';
					
					if($stageNo != $numStages && $logged_in)
					{
						if($team['qualified'] > 0)
						{
							$a_head = '<a  class="qualified" href="./index.php?qualify='.$team['team_id'].'#'.$stageNo.'">';
							
						}
						else
						{
							$a_head = '<a class="seeded"  href="./index.php?qualify='.$team['team_id'].'#'.$stageNo.'">';
						}
						
						$a_foot = '</a>';
					}
					
					$drawed_teams .= $a_head . '<span title="'.$team['dane'].'">'.$team['team_name'].'</span>'. $a_foot . '<br />';
				}
			}
			else
			{
				$d_team_ids = array();
				
				foreach($stageGroups[$stageNo][$i] as $team)
				{
					$drawed_teams .= $team['team_name'].'<br />';
					$d_team_ids[] = $team['team_id'];
				}
				
				foreach($matches as $match)
				{
					if(!in_array($match['home_team'], $d_team_ids))
					{
						$drawed_teams .= '<span class="qualified">' . get_team_name($match['home_team']). '</span><br />';
						$d_team_ids[] = $match['home_team'];
					}
					
					if(!in_array($match['away_team'], $d_team_ids))
					{
						$drawed_teams .= '<span class="qualified">' . get_team_name($match['away_team']). '</span><br />';
						$d_team_ids[] = $match['away_team'];
					}
				}
			}
		}
		
		$html[] = $drawed_teams . '</td>';
		$html[] = '<td class="dt">';
		
		
		$html[] = '<table class="matches">';
		foreach($matches as $match)
		{
			if($logged_in)
			{
				$score = '<a class="button score" href="javascript:setResult('.$match['match_id'].')" title="Set Result">Set</a>';
			}
			else
			{
				$score = 'v';
			}
			
			if($match['result'] != '' && $match['youtube'] != '')
			{
				$score = '<a class="button score" href="'.$match['youtube'].'" title="Watch Highlights" target="_blank"> '.$match['result'].' </a>';
			}
			else if($match['result'] != '')
			{
				$score = '<span title="Highlights coming soon">' . $match['result'] . '</span>';
			}
			
			$html[] = '<tr><td>' . $teams[$match['home_team']]['team_name'] . '</td><td>'.$score.'</td><td>' . $teams[$match['away_team']]['team_name'] . '</td></tr>';
		}
		
		$html[] = '</table>';
		$html[] = '</td>';
		$html[] = '</tr>';
	}
		
	
	$html[] = '</table>';
	
	$html[] = '</td></tr></table>';	
	$html[] = '</div>';
}

foreach($html as $line)
{
	echo $line . "\n";
}

?>
	<script>
	function setResult(match)
	{
		var x;

		var name=prompt("Set result of match","");

		if (name!=null)
	  {
		window.location = "./index.php?set="+match+"&result="+name;
	  }
	}
	</script>
	</div></div>
	</body>
</html>