<?php
//Setup variables
$finalTeams = 32;
$finalTeamsPerGroup = 4;
$qualTeamsPerGroup = 4;

$seedStart = 0.7;
$seedInterval = 0.1;
$seedEnd = 0.1;


//Load mysql
$dbhost 	= 'HOST';
$dbname 	= 'NAME';
$dbuser 	= 'USER';
$dbpasswd 	= 'PASSWORD';

//Switch to localhost, if applicaple
$whitelist = array('localhost', '127.0.0.1');

if(in_array($_SERVER['HTTP_HOST'], $whitelist)){
	$dbhost = 'localhost';
}

//Connect to database
$db = new mysql_db($dbname, $dbhost, $dbuser, $dbpasswd);

//Fetch all teams, ordered
$sql = "SELECT * FROM fifa_teams
			ORDER BY stage, team_rating DESC, team_sort";
			
$db->query($sql);

while($row = mysql_fetch_array($db->result))
{
	$teams[$row['team_id']] = $row;
}

$totalTeams = count($teams);

//Make sure the final amount of teams are not larger than the total amount of teams
if($totalTeams < $finalTeams)
{
	$finalTeams = $totalTeams;
}
	
//Calculate competition structure
$stages = knockoutStructure($totalTeams, $finalTeams, $seedStart, $seedInterval, $seedEnd);

$qualifiedTeams = array();
$seededTeams 	= array();
$stageGroups 	= array();

//Amount of stages
$numStages = count($stages);

if(isset($_GET['logout']) && $_GET['logout']=='true')
{
	// Unset all of the session variables.
	$_SESSION = array();

	// If it's desired to kill the session, also delete the session cookie.
	// Note: This will destroy the session, and not just the session data!
	if (ini_get("session.use_cookies")) {
		$params = session_get_cookie_params();
		setcookie(session_name(), '', time() - 42000,
			$params["path"], $params["domain"],
			$params["secure"], $params["httponly"]
		);
	}

	// Finally, destroy the session.
	session_destroy();
	
	header('Location: ./index.php');
}

if(isset($_GET['reset']) && $_GET['reset'] == 'true')
{
	$reset = true;
}
else
{
	$reset = false;
}

for($i = 1; $i <= $numStages; $i++)
{
	$seededTeams[$i] = array();
	$qualifiedTeams[$i] = array();
}

//If stages are not set (or on reset), place seeded teams in each stage
if(($reset || $db->exists('fifa_teams', 'stage = 0')) && $logged_in)
{ 
	//Clear stages and reselect teams
	$sql_data 	= array();
	$teams 		= array();
			
	$sql_data[] = array(
		'stage' 	=> 0,
		'qualified' => 0,
		'groupNo'	=> 0
	);
	
	if(!$db->update('fifa_teams', $sql_data, ''))
	{
		echo $db->error();
		exit();
	}
	
	$sql = 'TRUNCATE fifa_matches';
	
	$db->query($sql);
	
	//Fetch all teams, ordered
	$sql = "SELECT * FROM fifa_teams
			ORDER BY team_rating DESC, team_sort";
	$db->query($sql);

	while($row = mysql_fetch_array($db->result))
	{
		$teams[$row['team_id']] = $row;
	}

	//Seed teams, and place them in the correct stage
	foreach(array_reverse($stages, true) as $id => $stage)
	{
		$seededTeams[$id] = array();
		
		for($i=1; $i<= $stage['seeded']; $i++)
		{
			$seededTeam = array_shift($teams);
			$seededTeams[$id][$seededTeam['team_id']] = $seededTeam;
			
			//Update db with the new stage
			$sql_data = array();
			
			$sql_data[] = array(
				'stage' => $id + 1,
			);
			
			$where = 'team_id = ' . $seededTeam['team_id'] . '';
			
			if(!$db->update('fifa_teams', $sql_data, $where))
			{
				echo $db->error();
				exit();
			}
		}
	}
	
	header('Location: ./index.php');
}
else
{
	foreach($teams as $team)
	{
		if($team['qualified'] == 0)
		{
			$seededTeams[$team['stage']][$team['team_id']] = $team;
		}
	}
	
}

//Fetch all qualified teams, ordered
foreach($teams as $team)
{
	if($team['qualified'] > 0)
	{
		$qualifiedTeams[$team['stage']][$team['team_id']] = $team;
	}
}

//Get groups
foreach($teams as $team)
{
	$stageGroups[$team['stage']][$team['groupNo']][] = $team;
}


//Draw teams from given stage
if(isset($_GET['draw']) && !empty($stageGroups[$_GET['draw']]) && $logged_in)
{
	$drawStage = (int) $_GET['draw'];

	//Reset matches
	$sql = "DELETE FROM fifa_matches
			WHERE stage = " . $drawStage;
	
	$db->query($sql);
	
	
	$totalDrawTeams = count($seededTeams[$drawStage]) + count($qualifiedTeams[$drawStage]);
	
	//Determine the number of groups
	if($drawStage == $numStages)
	{
		//Final stage
		$numGroups = $totalDrawTeams / $finalTeamsPerGroup;
		$tPerGroup = $finalTeamsPerGroup;
	}
	elseif($drawStage == $numStages - 1)
	{
		//Penultimate stage
		$numGroups = $totalDrawTeams / $qualTeamsPerGroup;
		$tPerGroup = $qualTeamsPerGroup;
	}
	else
	{
		//All other stages
		$numGroups = $totalDrawTeams / 2;
		$tPerGroup = 2;
	}
	
	$stageGroups = array();
	
	$stageGroups[$drawStage] = draw_to_groups($seededTeams[$drawStage], $qualifiedTeams[$drawStage], $numGroups, $tPerGroup);
		
	//Insert groups into database
	foreach($stageGroups[$drawStage] as $id => $group)
	{
		$tmpGroup = $group;
		
		foreach($group as $teamId => $team)
		{	
			//Update db with the new stage
			$sql_data = array();
			
			$sql_data[] = array(
				'groupNo' => $id,
			);
			
			$where = 'team_id = ' . $team['team_id'];
			
			if(!$db->update('fifa_teams', $sql_data, $where))
			{
				echo $db->error();
				exit();
			}
			
			//Create matches
			
			unset($tmpGroup[$teamId]);
			$sql_data = array();
			
			foreach($tmpGroup as $team2)
			{
				$sql_data = array();
				$randTeam = array($team2, $team);
				
				shuffle($randTeam);
				
				$sql_data[] = array(
					'home_team' => $randTeam[0]['team_id'],
					'away_team' => $randTeam[1]['team_id'],
					'group_id'	=> $id,
					'stage'		=> $drawStage,
				);
				
				$sql_data[] = array(
					'home_team' => $randTeam[1]['team_id'],
					'away_team' => $randTeam[0]['team_id'],
					'group_id'	=> $id,
					'stage'		=> $drawStage,
				);
				
				if(!$db->insert('fifa_matches', $sql_data))
				{
					echo $db->error();
					exit();
				}
			}
			
		}
		
	}

	header('Location: ./index.php');
}

//Qualify team from given stage
if(isset($_GET['qualify']) && $logged_in)
{
	$team_id = (int) $_GET['qualify'];
	$sql_data[] = array(
		'qualified' => 'qualified + 1',
		'stage'		=> 'stage + 1'
	);
	
	$where = 'team_id = ' . $team_id;
	
	$sql = 'UPDATE fifa_teams SET qualified = qualified + 1, stage = stage + 1 WHERE team_id = '.$team_id;
	
	if(!$db->query($sql))
	{
		echo $db->error();
		exit();
	}
	header('Location: ./index.php');
}

//Qualify team from given stage
if(isset($_GET['set']) && $logged_in)
{
	$match_id = $_GET['set'];
	$result = $_GET['result'];
	
	$sql_data[] = array(
		'result' 	=> $result,
	);
	
	if(!$db->update('fifa_matches', $sql_data, 'match_id='.$match_id))
	{
		echo $db->error();
		exit();
	}
	
	header('Location: ./index.php');
}

function get_team_name($team_id)
{
	global $db;
	$row = $db->fields('fifa_teams', array('team_name'), 'team_id = ' . $team_id);
	
	return $row['team_name'];
}


function draw_to_groups($seededTeams, $qualifiedTeams, $numGroups, $tPerGroup)
{
	$groups = array();
	
	//Combine the two arrays
	$teamPool = $seededTeams;
	
	foreach($qualifiedTeams as $id => $team) 
	{ 
		$teamPool[$id] = $team; 
	} 
	
	aasort($teamPool, 'team_rating');
	
	$totalTeams = count($teamPool);
	
	//Create seed pots
	$pots = array();
	$i = 1;
	$k = 1;
	$potSize = $totalTeams / $tPerGroup;
	
	foreach($teamPool as $team)
	{
		if($k > $potSize)
		{
			$i++;
			$k = 1;
		}
		
		$pots[$i][] = $team;
		
		$k++;
	}
	
	//Prepare groups
	for($i=1; $i <= $numGroups; $i++)
	{
		$groups[$i] = array();
	}
	
	//Draw teams
	$pots = array_reverse($pots, true);
	
	foreach($pots as $pot)
	{
		shuffle($pot);
		$g = 1;
		$round = 0;
	
		foreach($pot as $team)
		{
			$picked = false;
			
			while(!$picked)
			{
				if($g > $numGroups)
				{
					$g = 1;
					$round ++;
				}
				
				if(count($groups[$g]) < $tPerGroup)
				{
					if(!in_group($team['team_country'],$groups[$g],'team_country') || $round > 2000)
					{
						$groups[$g][] = $team;
						$picked = true;
					}
				}
				
				$g++;
			}
		}
	}

	return $groups;
}


//Find if the same key is in the array of arrays
function in_group($value, $array, $key)
{
	
	foreach($array as $team)
	{
		if($team[$key] == $value)
		{
			return true;
		}
	}
	
	return false;
}

//Calculate competition structure based on seed
function knockoutStructure($totalTeams, $finalTeams, $seedStart, $seedInterval, $seedEnd, $reverse = true)
{
	$debug 		= false;
	$processing = true;
	
	//Prepare local processing variables
	$tTeams = $totalTeams;
	$nTeams = $finalTeams;
	
	//Knockout stages
	$i 			= 0;
	$stages 	= array();

	while($processing)
	{
		//Create an array for each stage
		$stages[$i] = array();
		
		//Set amount to eliminate
		$eliminate = 0;
		
		//The seed for this stage
		$seed = $seedStart - $i*$seedInterval;
		
		//Make sure the seed doesn't go below the seed threshold
		if($seed < $seedEnd)
		{
			$seed = $seedEnd;
		}
		
		//Amount of seeded teams are calculated from needed teams
		$seededTeams 	= floor($seed * $nTeams);
		
		//Amount of teams needed to qualify is equal to the remaining teams
		$qTeams 		= $nTeams - $seededTeams;
		
		//Remaining total teams are reduced
		$tTeams 		-= $seededTeams;
		
		//If there are enough teams to be halved, proceed to the next stage
		if($tTeams > 2* $qTeams)
		{
			$stages[$i]['seeded'] 	= $seededTeams;
			$stages[$i]['qualify'] 	= $qTeams;
		}
		else
		{
			//Calculate the last stage(s)
			
			//Put the seeded teams back in the total teams
			$tTeams 		+= $seededTeams;
			
			//The number of teams to be eliminated
			$eliminate = $tTeams - $nTeams;
			
			//Set the seeded and qualified teams for this stage
			$stages[$i]['seeded'] = $tTeams - 2*$eliminate;
			$stages[$i]['qualify'] = $eliminate;
			
			//If more than 0 teams need eliminated, create one previous stage
			if($eliminate > 0)
			{
				$stages[$i+1]['seeded'] = 2*$eliminate;
				$stages[$i+1]['qualify'] = 0;
			}
			
			//End processing
			$processing = false;
		}
		
		//Set needed teams for the previous round
		$nTeams = 2*$qTeams;

		if($debug)
		{
			print_r($stages[$i]);
			echo "<br>";
			if($tTeams > 2* $qTeams)
			{
				echo "Seed:" . $seed . "<br>";
				echo "seededTeams:" . $seededTeams . "<br>";
				echo "qTeams:" . $qTeams . "<br>";
			}
			echo "tTeams:" . $tTeams . "<br>";
			echo "nTeams:" . $nTeams . "<br>";
			echo "eliminate:" . $eliminate . "<br>";
			echo "<br><br>";
		}
		

		$i++;
	}

	if($debug && isset($stages[$i]))
	{
		print_r($stages[$i]);
		echo "<br>";
		echo "tTeams:" . $tTeams . "<br>";
		echo "nTeams:" . $nTeams . "<br>";
		echo "eliminate:" . $eliminate . "<br>";
		echo "<br><br>";
	}
	
	if($reverse)
	{
		return array_reverse($stages);
	}
	else
	{
		return $stages;
	}
}	


function aasort ($array, $key) {
    $sorter=array();
    $ret=array();
    reset($array);
    foreach ($array as $ii => $va) {
        $sorter[$ii]=$va[$key];
    }
    arsort($sorter);
    foreach ($sorter as $ii => $va) {
        $ret[$ii]=$array[$ii];
    }
    return $ret;
}
?>